import './style/index.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import echarts from './views/data/echarts';

// Element Plus
import 'element-plus/dist/index.css'
// 引入iconfont图标库
import '@/style/iconfont.scss'
// 样式重置
import 'normalize.css'
// 导入所有图标并全局注册
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// 引入Element Plus
import ElementPlus from 'element-plus'
// 引入Element Plus中文本地化
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
// 暗黑模式样式
import 'element-plus/theme-chalk/dark/css-vars.css'
// 解决首页事件捕获警告的问题
import 'default-passive-events'
// 添加音乐播放器插件
import AudioPlayer from 'vue3-audio-player'
import 'vue3-audio-player/dist/style.css'
const app = createApp(App)
app.use(store)
app.component('AudioPlayer', AudioPlayer)
app.use(router)
app.use(ElementPlus, {
  locale: zhCn,
})
app.config.globalProperties.$echarts = echarts
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
// 注册全局组件
app.mount('#app')
