import moment from 'moment' // 引入moment时间格式化插件
import 'moment/dist/locale/zh-cn' // moment本地化
// 十位时间戳(按秒计算)需要在moment后面添加unix
export const formatTime = (time: any) => {
  return moment.unix(time).format('YYYY-MM-DD HH:mm:ss')

}
// 格式化次数
export const formatNumber = (value: number): string => {
  if (value >= 100000000) {
    return Math.floor(value / 100000000) + '亿';
  } else if (value >= 10000) {
    return Math.floor(value / 10000) + '万';
  } else {
    return value.toString();
  }
}