// 解决按钮点击无法自动失焦的问题
export function buttonBlur (e: any) {
  let target = e.target
  if (target.nodeName === 'SPAN') {
    target = e.target.parentNode
  }
  // 强制失去焦点
  target.blur()
}