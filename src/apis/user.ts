import request from '@/service';
interface IData{
    username:string
    password:string
  }
  
  // 登录的接口  data={adminname:xxx,password}
  export function login (data:IData) {
    return request({
      url: '/login',
      method: 'post',
      data
    })
  }

  // 获取用户列表
  export function getUserList (data:any) {
    return request({
      url: '/users',
      params: data
    })
  }
//  添加用户
  export function addUser (data:any) {
    return request({
      url: '/users',
      method: 'post',
      data
    })
  }

  // 修改用户状态
  export function updateUserState (uid: Number,type:Boolean) {
    return request({
      url: '/users/'+uid+'/state/'+type,
      method: 'put',
    })
  }

  // 根据 ID 查询用户信息
  export function queryUser (id: any) {
    return request({
      url: '/users/'+id,
      method: 'get',
    })
  }
// 编辑用户提交
  export function editUser (id: Number,data:any) {
    return request({
      url: '/users/'+id,
      method: 'put',
      data
    })
  }

  // 删除单个用户
  export function deleteUser (id: Number) {
    return request({
      url: '/users/'+id,
      method: 'delete',
    })
  }

  // 设置用户角色权限
  export function setUserRole (id: any,rid: any) {
    return request({
      url: '/users/'+id+'/role/',
      method: 'put',
      data: rid
    })
  }
// 获取所有权限列表
  // export function getAllRoles ({type}:{type:any}) {// type的值list或tree
  //   return request({
  //     url: `/rights/+${type}`,
  //   })
  // }
  export function getAllRolesTree () {// type的值list或tree
    return request({
      url: `/rights/tree`,
    })
  }
  export function getAllRolesList () {// type的值list或tree
    return request({
      url: `/rights/list`,
    })
  }
  // 左侧菜单权限 
  export function getMenusRoles () {
    return request({
      url: '/menus'
    })
  }