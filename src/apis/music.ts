import { music } from '@/service/index'
// banner接口
export function getSwiper () {
  return music({
    url: '/banner',
  })
}

// 推荐歌单
export function getPersonalized () {
  return music({
    url: '/personalized?limit=20'
  })
}

// 获取歌单详情
export function getSongListDetail (data: any) {
  return music({
    url: '/playlist/detail',
    method: 'post',
    data
  })
}

// 获取歌单所有歌曲
export function getSongListAllMusic (data: any) {
  return music({
    url: '/playlist/track/all?limit=20',
    method: 'post',
    data
  })
}

// 获取音乐url
export function getMusicUrl (data: any) {
  return music({
    url: '/song/url/v1',
    method: 'post',
    data
  })
}
// 获取音乐详情
export function getMusicDetail (id: any) {
  return music({
    url: `/song/detail?ids=${id}`,
  })
}


// 获取歌单收藏者
export function getSongListCollector (data: any) {
  return music({
    url: '/playlist/subscribers',
    method: 'post',
    data
  })
}

// 获取用户详情
export function getUserDetail (data: any) {
  return music({
    url: '/user/detail',
    method: 'post',
    data
  })
}