import request from '@/service';
// 获取商品分类数据
export function getCategoriesList (data: any) {
  return request({
    url: '/categories',
    params: data
  })
}
// 编辑商品分类数据
export function editCategories (id: any, data: any) {
  return request({
    url: `/categories/${id}`,
    method: 'put',
    data
  })
}
// 添加商品分类数据
export function addCategories (data: any) {
  return request({
    url: `/categories`,
    method: 'post',
    data
  })
}
// 删除商品分类数据
export function removeCategories (id: any) {
  return request({
    url: `/categories/${id}`,
    method: 'delete',
  })
}

// 分类参数列表
export function getAttributesList (id: Number, data: Object) {
  return request({
    url: `/categories/${id}/attributes`,
    data
  })
}