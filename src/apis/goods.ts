import request from '@/service';
// import axios from 'axios'
// 获取商品列表
export function getGoodsList (data:any) {  // 有两个参数：pagenum和pagesize
    return request({
      url: '/goods',
      params: data,
    })
  }

// 删除某条商品
export function deleteCommodity (id:number) {  // 有1个参数：id
  return request({
    url: `/goods/${id}`,
    method: 'delete'
  })
}

// 新增商品
export function addGood (data:any) {  // 有两个参数：pagenum和pagesize
  return request({
    url: '/goods',
    method: 'post',
    data,
  })
}

// 编辑商品
export function editorGoods (id:any,data:any) {  // 有两个参数：pagenum和pagesize
  return request({
    url: '/goods/'+id,
    method: 'put',
    data,
  })
}

// 商品详情页获取
export function getDetail (id: Number) {
  return request({
      url: 'goods/' + id,
      method: 'get',
  })
}
// 搜索ID相关产品
export function searchId (id:any) {
  return request({
    url: 'goods/'+id,
    method: 'get',
  })
}

// 添加静态参数
// attr_name	参数名称	不能为空
// attr_sel	[only,many]	不能为空
// attr_vals	如果是 many 就需要填写值的选项，以逗号分隔	【可选参数】
export function addStaticParameters (id:any,data:any) {
  return request({
    url: `categories/${id}/attributes`,
    method: 'post',
    data,
  })
}

// 获取参数列表
export function getParameterList (id:any,params:any) {
  return request({
    url: `categories/${id}/attributes`,
    method: 'get',
    params,
  })
}

// 编辑参数列表 categories/:id/attributes/:attrId
export function editorParameterList (id:any,attrId:any,data:any) {
  return request({
    url: `categories/${id}/attributes/${attrId}`,
    method: 'put',
    data,
  })
}
// 删除参数列表某条数据 categories/:id/attributes/:attrid
export function deleteParameter (id:any,attrId:any) {
  return request({
    url: `categories/${id}/attributes/${attrId}`,
    method: 'delete',
  })
}