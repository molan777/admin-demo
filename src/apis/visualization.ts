import { service } from '@/service/index'
export const getVisualization = () => {
    return service({
        url: '/visualization'
    })
}