import request from '@/service';

// 角色列表
export function getRolesList () {
    return request({
      url: '/roles',
    })
  }

  // 添加角色
  export function addRoles (data:any) {
    return request({
      url: '/roles',
      method: 'post',
      data
    })
  }
  // 根据 ID 查询角色
  export function queryRolesId (id:any) {
    return request({
      url: '/roles/'+id,
    })
  }

  // 编辑提交角色
  export function editRoles (data:any) {
    return request({
      url: `/roles/${data.id}`,
      method: 'put',
      data
    })
  }
  // 删除角色
  export function deleteRoles (id: any) {
    return request({
      url: `/roles/${id}`,
      method: 'delete',
    })
  }
  // 改变角色授权
  export function changeRoles (data:any) {
    return request({
      url: `roles/${data.id}/rights`,
      method: 'post',
      data: {rids: data.rids }
    })
  }
  // 删除角色指定权限
  export function deleteRights (data:any) {
    return request({
      url: `roles/${data.id}/rights/${data.rightId}`,
      method: 'delete',
    })
  }