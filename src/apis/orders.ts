import request from '@/service';

// 订单数据列表
export function getOrdersList (data: any) {
  return request({
    url: '/orders',
    params: data
  })
}
// 修改订单状态
export function modifyOrders (id: any, data: any) {
  return request({
    url: '/orders/' + id,
    method: 'put',
    data
  })
}
// 查看订单详情
export function checkOrders (id: any) {
  return request({
    url: '/orders/' + id,
  })
}