import { ElMessage } from 'element-plus'
import { createRouter, createWebHistory } from 'vue-router'

export const routes = [
  {
    // 首页
    path: '/', name: 'home', label: '首页', component: () => import('@/views/Home.vue'),
    redirect: 'homecontent',
    children: [

      { path: 'homecontent', name: 'homecontent', component: () => import('@/views/HomeContent.vue') },
      {
        // 用户管理
        path: 'users',
        name: 'user',
        label: '用户管理',
        component: () => import('@/views/index/UserIndex.vue'),
        redirect: 'userlist',
        children: [
          // 用户列表
          { path: 'users', name: 'userlist', label: '用户列表', component: () => import('@/views/user/UserList.vue') },
        ]
      },
      {
        // 权限管理
        path: 'rights',
        name: 'role',
        label: '权限管理',
        component: () => import('@/views/index/RoleIndex.vue'),
        redirect: 'rolelist',
        children: [
          // 角色列表
          { path: 'roles', name: 'rolelist', label: '角色列表', component: () => import('@/views/role/RoleList.vue') },
          // 权限列表
          { path: 'rights', name: 'permissionlist', label: '权限列表', component: () => import('@/views/role/PermissionList.vue') },
        ]
      },

      {
        // 商品管理
        path: 'goods',
        name: 'product',
        label: '商品管理',
        component: () => import('@/views/index/ProductIndex.vue'),
        redirect: 'goods',
        children: [
          // 商品列表
          { path: 'goods', name: 'goods', component: () => import('@/views/product/ProductList.vue') },
          // 商品详情页
          { path: 'goods/:id', name: 'productdetail', label: '商品详情页', component: () => import('@/views/product/ProductDetail.vue') },
          // 分类参数
          { path: 'params', name: 'parameters', label: '分类参数', component: () => import('@/views/product/Parameters.vue') },
          // 商品分类
          { path: 'categories', name: 'categorylist', label: '商品分类', component: () => import('@/views/product/CategoryList.vue') },
        ]
      },
      {
        // 订单管理
        path: 'orders',
        name: 'order',
        label: '订单管理',
        component: () => import('@/views/index/OrderIndex.vue'),
        redirect: 'orderlist',
        children: [
          // 订单列表
          { path: 'orders', name: 'orderlist', label: '订单列表', component: () => import('@/views/order/OrderList.vue') },
          // 订单详情
          { path: 'orders/:id', name: 'orderdetail', label: '订单详情', component: () => import('@/views/order/OrderDetail.vue') },
        ]
      },
      {
        // 数据统计
        path: 'reports',
        name: 'data',
        label: '数据统计',
        component: () => import('@/views/index/DataIndex.vue'),
        redirect: 'datacenter',
        children: [
          // 数据中心
          { path: 'reports', name: 'datacenter', label: '数据中心', component: () => import('@/views/data/DataCenter.vue') }
        ]
      },
      {
        // 音乐中心
        path: 'music',
        name: 'music',
        label: '音乐中心',
        component: () => import('@/views/index/MusicIndex.vue'),
        redirect: 'findmusic',
        children: [
          // 发现音乐
          { path: 'findmusic', name: 'findmusic', label: '发现音乐', component: () => import('@/views/music/Findmusic.vue') },
          // 用户页面
          { path: 'musicuser', name: 'musicuser', label: '用户详情', component: () => import('@/views/music/User.vue') },
          {
            // 歌单详情
            path: 'songlistdetail', name: 'songlistdetail', label: '歌单详情', component: () => import('@/views/music/SongListDetail.vue'),
            redirect: 'songlistdetail/songlist',
            children: [
              // 歌曲列表
              { path: 'songlist', name: 'songlist', label: '歌曲列表', component: () => import('@/views/music/SongList.vue') },
              // 收藏者
              { path: 'collector', name: 'collector', label: '收藏者', component: () => import('@/views/music/Collector.vue') },
            ]
          }
        ]
      }
    ]
  },
  // 登录页面
  { path: '/login', name: 'login', component: () => import('@/views/Login.vue') },
  // 404
  { path: '/404', component: () => import('@/views/404.vue') },
  { path: '/:catchAll(.*)', redirect: '/404' }
]

const router = createRouter({
  routes,
  history: createWebHistory(import.meta.env.BASE_URL)
})

// 借助全局前置路由守卫进行登录拦截
router.beforeEach((to: any) => {
  if (to.name === 'login') {  // 如果跳转的是登录页面，并且本地存储有token
    if (localStorage.getItem('token')) {
      ElMessage('登录状态有效，不需要重新登录!')
      return { name: 'home' }
    }
  }
  return true
})

export default router
