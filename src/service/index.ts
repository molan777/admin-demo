import axios from 'axios'
import router from '@/router';
import { ElMessage } from 'element-plus'
// 引入加载进度条依赖
import NProgress from 'nprogress'
// 引入加载进度条样式
import 'nprogress/nprogress.css'
// 判断当前的环境 开发环境 || 生产环境
switch (process.env.NODE_ENV) {
  case 'development':
    axios.defaults.baseURL = 'http://wangshoubin.top:8888/api/private/v1'
    break;
  case 'production':
    axios.defaults.baseURL = 'http://wangshoubin.top:8888/api/private/v1'
    break;
}



// axios请求前的拦截器
axios.interceptors.request.use((config: any) => {
  NProgress.start()
  if (localStorage.getItem('token')) {
    const token = localStorage.getItem('token')
    config.headers.Authorization = token
  }
  config.headers = {
    ...config.headers,
    'Content-Type': 'application/json; charset=utf-8',
  }
  return config
})

// axios响应后的拦截器
// 可以对于后端返回的数据进行相关的业务处理或者判断
// 第二个回调函数中，可以对于后端返回的一些错误的状态码进行相关的业务判断，例如token失效的时候。
// http://www.cnblogs.com/chaoyuehedy/p/9931146.html
axios.interceptors.response.use(res => {
  NProgress.done()
  if (res?.data?.data?.token) {
    localStorage.setItem('token', res.data.data.token)
  }
  // console.log(res);
  // token失效返回的是400，但有其他操作也会返回400，所以使用string，跳转登录
  if (res?.data?.meta?.msg === "无效token") {
    localStorage.removeItem('token')
    router.push('/login')
  }
  return res.data
})
export default axios





// 配置新建一个首页大屏的axios 实例
export const service = axios.create({
  baseURL: "https://api.imooc-web.lgdsunday.club/api",
  timeout: 5000,
});
service.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    config.headers.icode = 'input your icode'
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    // 请求错误的操作，返回报错信息
    return Promise.reject(error);
  }

);
// 新建网易云的axios实例
export const music = axios.create({
  baseURL: "https://autumnfish.cn/",
  // timeout: 5000,
});
music.interceptors.request.use((config: any) => {
  // const cookie = localStorage.getItem('cookie');
  // if (cookie) {
  //   config.headers.cookie = cookie
  // }
  // 定义时间戳
  const timestamp = Date.now()

  // 如果config.params对象不存在 则创建一个空对象 保证后续添加参数时不会报错
  if (!config.params) {
    config.params = {}
  }
  // 在请求体中携带时间戳
  config.params = {
    ...config.params,
    timestamp
  }
  return config
},
  error => {
    return Promise.reject(error)
  }
)


