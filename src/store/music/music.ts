export default {
  namespaced: true, // 开启命名空间
  state () {
    return {
      songListId: '', // 歌单id
      userId: '',  // 用户id
      search: '',  // 表单搜索的内容
      musicUrl: '',  // 音乐地址
      musicInfo: {},  // 音乐信息 存储播放器的音乐地址 名称 图片地址
    };
  },
  mutations: { // 同步更改
    updateSongListId (state: any, payload: any) {
      state.songListId = payload
    },
    updateSearch (state: any, payload: any) {
      state.search = payload
    },
    updateUserId (state: any, payload: any) {
      state.userId = payload
    },
    updateMusicUrl (state: any, payload: any) {
      state.musicUrl = payload
    },
    updateMusicInfo (state: any, payload: any) {
      state.musicInfo = payload
    },
  }
}
