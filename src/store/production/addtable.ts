export default{
  namespaced: true, // 开启了命名空间
  state () {
    return {
      active: 0,
      addGood: {},
      goodDetail: {}
    };
  },
  mutations: { // 同步更改
    setActive (state:any,active:any) {
      state.active=active
    },
    addGoddsFn (state:any,add:any) { // 步骤器，每一步的数据积累在一个对象中
      state.addGood={...state.addGood,...add}
    },
    addGoodsClear (state:any) { // 清空addGood，防止脏数据
      state.addGood={}
    },
    addDeatailFn (state:any,add:any) {
      state.goodDetail=add
    },
  },
  actions: {
  }
}
