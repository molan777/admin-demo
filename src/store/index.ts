import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate"; // 引入vuex持久化插件
import addTable from './production/addtable'
import music from './music/music'
const store = createStore({
  modules: {
    addTable,
    music
  },
  state () {
    return {
      userInfo: null || [],
      currentPath: "",
    };
  },
  mutations: {
    // 同步的更改vuex的state
    updateUserInfo (state: any, payload: any) {
      state.userInfo = payload;
    },
    // 记录每次用户退出之前的路径
    updateCurrentPath (state: any, payload: any) {
      state.currentPath = payload
    }
  },
  plugins: [
    createPersistedState({
      // 需要持久化的状态
      paths: ["userInfo", "currentPath", "addTable", "music"],
    }),
  ],
});

export default store;

// 导入方式：举例
// 选项式

// import { useStore } from 'vuex';
// const store = useStore() // 步骤条
// const active= computed(()=>store.state.addTable.active)// 获取vuex的state|getter需要借助computed才可以