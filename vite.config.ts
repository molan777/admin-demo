import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    proxy: {
      '/v1': {
        target: 'http://wangshoubin.top:8888/api/private/v1', // 后端服务器的地址
        // 这是后端服务器的地址，请求将被代理到这个地址。
        changeOrigin: true, // 是否改变请求的原始主机头为目标URL
        // 这个选项用于控制是否改变请求的原始主机头为目标URL。通常需要设置为true，以确保跨域请求正确处理。
        ws: true, // 是否启用WebSocket代理
        rewrite: path => path.replace(/^\/v1/, '') // 重写请求路径，将请求中的'/v1'替换为空字符串
      },
    },
  }
})
